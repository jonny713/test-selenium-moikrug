var wd = require('selenium-webdriver'),
    By = wd.By,
	countV = 0;

var SELENIUM_HOST = 'http://localhost:4444/wd/hub';
var URL = 'http://www.moikrug.ru';

var client = new wd.Builder()
   .usingServer(SELENIUM_HOST)
   .withCapabilities({ browserName: 'firefox' })
   .build();

client.get(URL).then(function() {
    client.executeScript("document.getElementsByName('city_id')[0].setAttribute('value', '678')");      //вводим id нужного города, для Москвы это 678
    client.findElement(By.name('q') ).sendKeys('Ruby on Rails');                                        //запрос для поиска
	client.findElement(By.css('.inline-landing-from') ).submit().then(_ => recursivo (client));         //жмем поиск и переходим в рекурсивную функцию
});

function recursivo (client){                                          //рекурсивная функция для просмотра всех страниц выборки
	client.sleep(2000);                                               //ожидание прогрузки DOM
	client.findElement(By.id('jobs_list')).then( jobList => {         //выборка и вывод необходимых элементов
		jobList.findElements(By.css('.title')).then( arrayJob => {     
			arrayJob.forEach(item => {item.getAttribute('title').then( str => console.log( str ) ); countV++} );
		});
	});
	client.findElement(By.css('a.next_page') ).click()
		.then(_ => recursivo (client))                                //переход на новую страницу
		.catch(_ => console.log("number of vacancies -> "+countV));    //новых страниц нет - выводим общее кол-во вакансий
}